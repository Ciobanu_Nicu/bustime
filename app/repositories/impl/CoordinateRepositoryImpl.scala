package repositories.impl

import javax.inject.Inject
import models.Coordinate
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoApi
import play.modules.reactivemongo.json._
import reactivemongo.api.{Cursor, FailoverStrategy, ReadPreference}
import reactivemongo.play.json.collection.JSONCollection
import repositories.CoordinateRepository

import scala.concurrent.{ExecutionContext, Future}

class CoordinateRepositoryImpl @Inject()(reactiveMongoApi: ReactiveMongoApi) (implicit ec: ExecutionContext) extends CoordinateRepository{


  def collection: Future[JSONCollection] = {
    reactiveMongoApi.database.map(_.collection("coordinate", FailoverStrategy.default))
  }

  def create(coordinate: Coordinate) = {
    val newID = coordinate.entry_id
    val r = coordinate.copy(
      entry_id = newID
    )
    val newCoordinate = for {
      _ <- collection.flatMap(_.insert[Coordinate](r))
    } yield Some(r)
    newCoordinate
  }

  override def findById(entry_id: Int): Future[Option[Coordinate]] = {
    collection.flatMap(_.find(Json.obj("entry_id" -> entry_id)).one[Coordinate](ReadPreference.primary))
  }

  override def findAll(): Future[List[Coordinate]] = {
    collection.flatMap(_.find(Json.obj())
      .cursor[Coordinate](ReadPreference.primary)
      .collect[List](Int.MaxValue, Cursor.ContOnError[List[Coordinate]]()))
  }


  override def delete(entry_id: Int) = {
    for{
      d <- collection.flatMap(_.remove(Json.obj("entry_id" -> entry_id)))
    } yield {
      if (d.ok){
        d
      }
      else d
    }
  }

  override def findByTz(created_at: String): Future[Option[Coordinate]] = {
    val s = Json.obj("created_at" -> created_at)
    collection.flatMap(_.find(s).one[Coordinate](ReadPreference.primary))
  }



}
