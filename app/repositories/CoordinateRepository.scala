package repositories
import models.Coordinate
import reactivemongo.api.commands.WriteResult

import scala.concurrent.Future

trait CoordinateRepository {

  def create(coordinate: Coordinate): Future[Option[Coordinate]]

  def findById(entry_id: Int): Future[Option[Coordinate]]

  def findByTz(created_at: String): Future[Option[Coordinate]]

  def delete(entry_id: Int): Future[WriteResult]

  def findAll(): Future[List[Coordinate]]

}
