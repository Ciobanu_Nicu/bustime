package models

import play.api.libs.functional.syntax._
import play.api.libs.json._
import reactivemongo.bson.BSONObjectID

import scala.util.Try

/**
  * The Coordinate model is created with the respective fields on IoT Cloud
  *
  * @param created_at - is the time when the data was created
  * @param entry_id   - represents the id of the data at the time of the request
  * @param field1     - latitude
  * @param field2     - longitude
  */

case class Coordinate(created_at: String,
                      entry_id: Int,
                      field1: String,
                      field2: String)


object Coordinate  {

  implicit val coordinateReads: Reads[Coordinate] =(
    (JsPath \ "created_at").read[String] and
      (JsPath \ "entry_id").read[Int] and
      (JsPath \ "field1").read[String] and
      (JsPath \ "field2").read[String]
    ) (Coordinate.apply _)

  implicit val coordinateWrites: Writes[Coordinate] =(
    (JsPath \ "created_at").write[String] and
      (JsPath \ "entry_id").write[Int] and
      (JsPath \ "field1").write[String] and
      (JsPath \ "field2").write[String]
    ) (unlift(Coordinate.unapply))

  implicit val coordinateFormat: OFormat[Coordinate] = Json.format[Coordinate]
  implicit val coordFormat: Format[Coordinate] = Format(coordinateReads, coordinateWrites)
  implicit val coordinateRead = Json.writes[Coordinate]

  //  HubApp => UserRepresentation

/*  implicit val coordinateListWriter: Writes[List[Coordinate]] = (list: List[Coordinate]) =>
    JsArray(list.toArray.map(Json.toJson(_)(coordinateWrites.asInstanceOf[Writes[AnyRef]])))*/

}
