package controllers

import javax.inject.Inject
import play.api.libs.json.{JsResult, Json}
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, ControllerComponents}
import services.CoordinateService
import models.Coordinate
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class CoordinateController @Inject()
(cc: ControllerComponents, coordinateService: CoordinateService, ws: WSClient)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def getBusCoordinates = Action.async {

    val gpsDataCloud = ws.url("https://api.thingspeak.com/channels/xxxxxx/feeds.json?results=1").get()

    val listOfGPSData = for{
      data <- gpsDataCloud
    } yield data

    val finalResult = listOfGPSData.map { response =>
      val coordinates = (response.json \ "feeds").as[List[Coordinate]]
//      coordinateService.create(coordinates.head)
      Ok(Json.toJson(coordinates.head))
    }

    finalResult
  }


  def getTimeBetweenDestinations(destLatitude: Double, destLongitude: Double) = Action.async {

    val gpsDataCloud = ws.url("https://api.thingspeak.com/channels/xxxxxx/feeds.json?results=1").get()

    val busGPSLocation = for{
      data <- gpsDataCloud
    } yield data

    busGPSLocation.map{response =>
        val coordinates = (response.json \ "feeds").as[List[Coordinate]]
//        coordinateService.create(coordinates.head)
        val originLat = coordinates.head.field1.toDouble / 100.00
        val originLong = coordinates.head.field2.toDouble / 100.00

      val gpsDataMaps = ws.url(s"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$originLat,$originLong&destinations=$destLatitude,$destLongitude&key=xxxxxx").get()

      val futureResponse = for{
          gpsMapData <- gpsDataMaps
        } yield gpsMapData.body

      val frontresponse = Await.result(futureResponse, Duration.Inf)

        Ok(Json.toJson("Latitude: " + originLat + " Longitude: " + originLong + " Maps Response: " +frontresponse))
    }

  }


  /**
    * Extract the data and time from timestamp field - created_at
    *
    * @param ts - must be converted to the Double or Long type before being called in funtion
    * @return
    */

  def getTime(ts: Double) = {

    val dt = new DateTime(ts.toLong * 1000)
    dt

  }


  def getDate(ts: Long) = {

    val dtFormatter = DateTimeFormat.forPattern("dd-MM-yyyy")
    val res = dtFormatter.print(ts * 1000)
    res

  }

  def createCoornidate() = Action.async(parse.json) { implicit request =>
    request.body.validate[Coordinate].fold(
      _ => Future.successful(BadRequest(Json.obj("status" -> "Invalid"))),
      message => {
        coordinateService
          .create(message)
          .map(Json.toJson(_))
          .map(Created(_))
      }
    )
  }

  def findById(entry_id: Int) = Action.async { implicit request =>
    coordinateService
      .findById(entry_id)
      .map(Json.toJson(_))
      .map(Ok(_))

  }


  def findAllCoordinates() = Action.async { implicit request =>
    coordinateService
      .findAll()
      .map(Json.toJson(_))
      .map(Ok(_))
  }

  def findByTz(created_at: String) = Action.async { implicit request =>
    coordinateService
      .findByTz(created_at)
      .map(Json.toJson(_))
      .map(Ok(_))
  }

  def delete(entry_id: Int) = Action.async { implicit request =>
    coordinateService
      .delete(entry_id)
      .map(_ => Ok)
  }

}
