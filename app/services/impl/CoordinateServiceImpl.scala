package services.impl

import javax.inject.Inject
import models.Coordinate
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.FailoverStrategy
import reactivemongo.play.json.collection.JSONCollection
import repositories.CoordinateRepository
import services.CoordinateService

import scala.concurrent.{ExecutionContext, Future}


class CoordinateServiceImpl @Inject()(reactiveMongoApi: ReactiveMongoApi, coordinateRepository: CoordinateRepository)(implicit ex: ExecutionContext) extends CoordinateService{

  def collection: Future[JSONCollection] = {
    reactiveMongoApi.database.map(_.collection("coordinate", FailoverStrategy.default))
  }

  def create(coordinate: Coordinate) = {
    coordinateRepository.create(coordinate)
  }

  override def findById(entry_id: Int): Future[Option[Coordinate]] = {
    coordinateRepository.findById(entry_id)
  }


  override def findAll(): Future[List[Coordinate]] = {
    for {
      u <- coordinateRepository.findAll()
    } yield u
  }

  override def delete(entry_id: Int) = {
    coordinateRepository.delete(entry_id)
  }


  override def findByTz(created_at: String): Future[Option[Coordinate]] = {
    coordinateRepository.findByTz(created_at)
  }

}
