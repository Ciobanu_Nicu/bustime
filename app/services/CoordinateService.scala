package services

import reactivemongo.api.commands.WriteResult
import models.Coordinate
import scala.concurrent.Future

trait CoordinateService {

  def create(coordinate: Coordinate): Future[Option[Coordinate]]

  def findById(entry_id: Int): Future[Option[Coordinate]]

  def findAll(): Future[List[Coordinate]]

  def findByTz(created_at: String): Future[Option[Coordinate]]

  def delete(entry_id: Int): Future[WriteResult]

}
