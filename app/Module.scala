import com.google.inject.AbstractModule

class Module extends AbstractModule {
  override def configure() = {
    bind(classOf[services.CoordinateService]).to(classOf[services.impl.CoordinateServiceImpl])
    bind(classOf[repositories.CoordinateRepository]).to(classOf[repositories.impl.CoordinateRepositoryImpl])
  }
}