// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/BusTime/conf/routes
// @DATE:Mon Dec 03 20:32:01 EET 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.modules.reactivemongo.PathBindables._
import _root_.play.modules.reactivemongo.PathBindables._

// @LINE:7
package controllers {

  // @LINE:9
  class ReverseCoordinateController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def getBusCoordinates(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "getBusCoordinates")
    }
  
    // @LINE:13
    def getTimeBetweenDestinations(destLatitude:Double, destLongitude:Double): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "getTimeBetweenDestinations/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Double]].unbind("destLatitude", destLatitude)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Double]].unbind("destLongitude", destLongitude)))
    }
  
    // @LINE:17
    def findByTz(created_at:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "created_at/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("created_at", created_at)))
    }
  
    // @LINE:21
    def delete(entry_id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("entry_id", entry_id)))
    }
  
    // @LINE:15
    def findById(entry_id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "entry_id/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("entry_id", entry_id)))
    }
  
    // @LINE:9
    def createCoornidate(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "createCoordinate")
    }
  
    // @LINE:19
    def findAllCoordinates(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "allCoordinates")
    }
  
  }

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:24
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }


}
