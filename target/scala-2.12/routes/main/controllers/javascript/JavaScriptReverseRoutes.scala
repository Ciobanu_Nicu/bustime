// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/BusTime/conf/routes
// @DATE:Mon Dec 03 20:32:01 EET 2018

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.modules.reactivemongo.PathBindables._
import _root_.play.modules.reactivemongo.PathBindables._

// @LINE:7
package controllers.javascript {

  // @LINE:9
  class ReverseCoordinateController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def getBusCoordinates: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CoordinateController.getBusCoordinates",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getBusCoordinates"})
        }
      """
    )
  
    // @LINE:13
    def getTimeBetweenDestinations: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CoordinateController.getTimeBetweenDestinations",
      """
        function(destLatitude0,destLongitude1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getTimeBetweenDestinations/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Double]].javascriptUnbind + """)("destLatitude", destLatitude0)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Double]].javascriptUnbind + """)("destLongitude", destLongitude1))})
        }
      """
    )
  
    // @LINE:17
    def findByTz: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CoordinateController.findByTz",
      """
        function(created_at0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "created_at/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("created_at", created_at0))})
        }
      """
    )
  
    // @LINE:21
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CoordinateController.delete",
      """
        function(entry_id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("entry_id", entry_id0))})
        }
      """
    )
  
    // @LINE:15
    def findById: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CoordinateController.findById",
      """
        function(entry_id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "entry_id/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("entry_id", entry_id0))})
        }
      """
    )
  
    // @LINE:9
    def createCoornidate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CoordinateController.createCoornidate",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "createCoordinate"})
        }
      """
    )
  
    // @LINE:19
    def findAllCoordinates: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CoordinateController.findAllCoordinates",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "allCoordinates"})
        }
      """
    )
  
  }

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:24
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }


}
