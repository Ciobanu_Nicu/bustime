// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/BusTime/conf/routes
// @DATE:Mon Dec 03 20:32:01 EET 2018

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseCoordinateController CoordinateController = new controllers.ReverseCoordinateController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseCoordinateController CoordinateController = new controllers.javascript.ReverseCoordinateController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
  }

}
