// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/BusTime/conf/routes
// @DATE:Mon Dec 03 20:32:01 EET 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
