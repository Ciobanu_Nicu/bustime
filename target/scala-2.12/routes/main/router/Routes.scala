// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/BusTime/conf/routes
// @DATE:Mon Dec 03 20:32:01 EET 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.modules.reactivemongo.PathBindables._
import _root_.play.modules.reactivemongo.PathBindables._

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HomeController_1: controllers.HomeController,
  // @LINE:9
  CoordinateController_0: controllers.CoordinateController,
  // @LINE:24
  Assets_2: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HomeController_1: controllers.HomeController,
    // @LINE:9
    CoordinateController_0: controllers.CoordinateController,
    // @LINE:24
    Assets_2: controllers.Assets
  ) = this(errorHandler, HomeController_1, CoordinateController_0, Assets_2, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_1, CoordinateController_0, Assets_2, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """createCoordinate""", """controllers.CoordinateController.createCoornidate"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getBusCoordinates""", """controllers.CoordinateController.getBusCoordinates"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getTimeBetweenDestinations/""" + "$" + """destLatitude<[^/]+>/""" + "$" + """destLongitude<[^/]+>""", """controllers.CoordinateController.getTimeBetweenDestinations(destLatitude:Double, destLongitude:Double)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """entry_id/""" + "$" + """entry_id<[^/]+>""", """controllers.CoordinateController.findById(entry_id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """created_at/""" + "$" + """created_at<[^/]+>""", """controllers.CoordinateController.findByTz(created_at:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """allCoordinates""", """controllers.CoordinateController.findAllCoordinates"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """delete/""" + "$" + """entry_id<[^/]+>""", """controllers.CoordinateController.delete(entry_id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_1.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_CoordinateController_createCoornidate1_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("createCoordinate")))
  )
  private[this] lazy val controllers_CoordinateController_createCoornidate1_invoker = createInvoker(
    CoordinateController_0.createCoornidate,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CoordinateController",
      "createCoornidate",
      Nil,
      "POST",
      this.prefix + """createCoordinate""",
      """""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_CoordinateController_getBusCoordinates2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getBusCoordinates")))
  )
  private[this] lazy val controllers_CoordinateController_getBusCoordinates2_invoker = createInvoker(
    CoordinateController_0.getBusCoordinates,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CoordinateController",
      "getBusCoordinates",
      Nil,
      "GET",
      this.prefix + """getBusCoordinates""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_CoordinateController_getTimeBetweenDestinations3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getTimeBetweenDestinations/"), DynamicPart("destLatitude", """[^/]+""",true), StaticPart("/"), DynamicPart("destLongitude", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CoordinateController_getTimeBetweenDestinations3_invoker = createInvoker(
    CoordinateController_0.getTimeBetweenDestinations(fakeValue[Double], fakeValue[Double]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CoordinateController",
      "getTimeBetweenDestinations",
      Seq(classOf[Double], classOf[Double]),
      "GET",
      this.prefix + """getTimeBetweenDestinations/""" + "$" + """destLatitude<[^/]+>/""" + "$" + """destLongitude<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_CoordinateController_findById4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("entry_id/"), DynamicPart("entry_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CoordinateController_findById4_invoker = createInvoker(
    CoordinateController_0.findById(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CoordinateController",
      "findById",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """entry_id/""" + "$" + """entry_id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_CoordinateController_findByTz5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("created_at/"), DynamicPart("created_at", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CoordinateController_findByTz5_invoker = createInvoker(
    CoordinateController_0.findByTz(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CoordinateController",
      "findByTz",
      Seq(classOf[String]),
      "GET",
      this.prefix + """created_at/""" + "$" + """created_at<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_CoordinateController_findAllCoordinates6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("allCoordinates")))
  )
  private[this] lazy val controllers_CoordinateController_findAllCoordinates6_invoker = createInvoker(
    CoordinateController_0.findAllCoordinates,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CoordinateController",
      "findAllCoordinates",
      Nil,
      "GET",
      this.prefix + """allCoordinates""",
      """""",
      Seq()
    )
  )

  // @LINE:21
  private[this] lazy val controllers_CoordinateController_delete7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("delete/"), DynamicPart("entry_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CoordinateController_delete7_invoker = createInvoker(
    CoordinateController_0.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CoordinateController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """delete/""" + "$" + """entry_id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_Assets_versioned8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned8_invoker = createInvoker(
    Assets_2.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_1.index)
      }
  
    // @LINE:9
    case controllers_CoordinateController_createCoornidate1_route(params@_) =>
      call { 
        controllers_CoordinateController_createCoornidate1_invoker.call(CoordinateController_0.createCoornidate)
      }
  
    // @LINE:11
    case controllers_CoordinateController_getBusCoordinates2_route(params@_) =>
      call { 
        controllers_CoordinateController_getBusCoordinates2_invoker.call(CoordinateController_0.getBusCoordinates)
      }
  
    // @LINE:13
    case controllers_CoordinateController_getTimeBetweenDestinations3_route(params@_) =>
      call(params.fromPath[Double]("destLatitude", None), params.fromPath[Double]("destLongitude", None)) { (destLatitude, destLongitude) =>
        controllers_CoordinateController_getTimeBetweenDestinations3_invoker.call(CoordinateController_0.getTimeBetweenDestinations(destLatitude, destLongitude))
      }
  
    // @LINE:15
    case controllers_CoordinateController_findById4_route(params@_) =>
      call(params.fromPath[Int]("entry_id", None)) { (entry_id) =>
        controllers_CoordinateController_findById4_invoker.call(CoordinateController_0.findById(entry_id))
      }
  
    // @LINE:17
    case controllers_CoordinateController_findByTz5_route(params@_) =>
      call(params.fromPath[String]("created_at", None)) { (created_at) =>
        controllers_CoordinateController_findByTz5_invoker.call(CoordinateController_0.findByTz(created_at))
      }
  
    // @LINE:19
    case controllers_CoordinateController_findAllCoordinates6_route(params@_) =>
      call { 
        controllers_CoordinateController_findAllCoordinates6_invoker.call(CoordinateController_0.findAllCoordinates)
      }
  
    // @LINE:21
    case controllers_CoordinateController_delete7_route(params@_) =>
      call(params.fromPath[Int]("entry_id", None)) { (entry_id) =>
        controllers_CoordinateController_delete7_invoker.call(CoordinateController_0.delete(entry_id))
      }
  
    // @LINE:24
    case controllers_Assets_versioned8_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned8_invoker.call(Assets_2.versioned(path, file))
      }
  }
}
