name := """reactive-mongo-learn"""
organization := "com.handy.learn"

version := "1.0-SNAPSHOT"

import play.sbt.routes.RoutesKeys
RoutesKeys.routesImport += "play.modules.reactivemongo.PathBindables._"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

/* ReactiveMongo */
libraryDependencies += "org.reactivemongo" %% "play2-reactivemongo" % "0.12.7-play26"
libraryDependencies += "org.reactivemongo" %% "reactivemongo-akkastream" % "0.12.7"


import play.sbt.routes.RoutesKeys

RoutesKeys.routesImport += "play.modules.reactivemongo.PathBindables._"


libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.7"
)

libraryDependencies += "org.edla" %% "tmdb-async-client" % "1.2.1"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.10"

libraryDependencies += ws

libraryDependencies += filters
